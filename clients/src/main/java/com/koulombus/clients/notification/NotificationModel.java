package com.koulombus.clients.notification;

import java.time.Instant;
import java.util.UUID;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
public class NotificationModel {

    @Id
    private String notificationId = UUID.randomUUID().toString();
    private String message;
    private String sender;
    private Instant sentAt;
    private String toCustomerEmail;
    private String toCustomerId;
}
