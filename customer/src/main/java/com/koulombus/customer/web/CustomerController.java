package com.koulombus.customer.web;

import java.util.List;

import com.koulombus.customer.model.CustomerModel;
import com.koulombus.customer.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PostMapping
    public CustomerModel registerCustomer(@RequestBody CustomerModel model) {
        return customerService.registerCustomer(model);
    }

    @GetMapping
    public List<CustomerModel> getAllCustomers() {
        return customerService.findAllCustomers();
    }

}
