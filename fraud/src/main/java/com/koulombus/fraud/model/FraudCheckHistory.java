package com.koulombus.fraud.model;

import java.time.Instant;
import java.util.UUID;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
public class FraudCheckHistory {

    @Id
    private String id = UUID.randomUUID().toString();
    private String customerId;
    private Boolean isFraudster;
    private Instant createdAt;
}
