package com.koulombus.fraud.services;

import java.time.Instant;

import com.koulombus.fraud.model.FraudCheckHistory;
import com.koulombus.fraud.model.FraudCheckHistoryRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class FraudCheckService {

    private FraudCheckHistoryRepository fraudCheckHistoryRepository;

    public Boolean setFraudulentCustomer(String customerId) {
        log.info("Save fraud for requested customer.");
        FraudCheckHistory fraudCheckHistory = new FraudCheckHistory();
        fraudCheckHistory.setCustomerId(customerId);
        fraudCheckHistory.setIsFraudster(isFraudulentCustomer(customerId));
        fraudCheckHistory.setCreatedAt(Instant.now());
        fraudCheckHistoryRepository.save(fraudCheckHistory);
        return fraudCheckHistory.getIsFraudster();
    }

    /*
     * Do any fraud check here!
     */
    public Boolean isFraudulentCustomer(String customerId) {
        log.info("Do some fraud check here with customer id: {}", customerId);
        return false;
    }
}
